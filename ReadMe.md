# SpelunkyTrainer

### options

- [x] godmode 
- [x] instakill
- [x] give 9999999 money on pickup
- [x] unlimited bombs
- [x] unlimited ropes
- [x] fly hack/gravity
- [ ] no collisions
- [ ] freezing the ghost timer (currently broken)

### Screenshot:
![alt text](screenshots/screenshot.jpg "screenshot")

### Librarys

We use Memory.dll from https://github.com/erfg12/memory.dll/