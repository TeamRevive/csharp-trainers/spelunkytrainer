﻿using Memory;
using spelunkytrainer.Properties;
using spelunkytrainer.cheats;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Reflection;
using System.Globalization;

namespace spelunkytrainer
{
    public partial class Window : Form
    {
        private GodmodeCheat godmode;
        private InstakillCheat instakill;
        private MoneyCheat money;
        private BombCheat bomb;
        private RopeCheat ropec;
        private FlyCheat flycheat;
        private GhostTimerCheat ghost;

        private bool hooked = false;

        private Mem m;

        public Window()
        {
            InitializeComponent();
        }


        private void Window_Load(object sender, EventArgs e)
        {

            this.m = new Mem();

            this.hooksearch.RunWorkerAsync();
            HotKeysz.RegisterKey(this, Keys.F.GetHashCode());
            HotKeysz.RegisterKey(this, Keys.F1.GetHashCode());
            HotKeysz.RegisterKey(this, Keys.F2.GetHashCode());

            this.fadeControl1.Start();
            this.fadeControl1.FadeDone += new EventHandler(delegate (object s, EventArgs ev) {
                //add here your other data after the opacity animation.
                this.moveableWindow1.ControlTargets = new Control[] { this.header };
                
            });
        }


        protected override void WndProc(ref Message m)
        {
            base.WndProc(ref m);

            if (m.Msg == 0x0312)
            {
                /* Note that the three lines below are not needed if you only want to register one hotkey.
                 * The below lines are useful in case you want to register multiple keys, which you can use a switch with the id as argument, or if you want to know which key/modifier was pressed for some particular reason. */

                Keys key = (Keys)(((int)m.LParam >> 16) & 0xFFFF);                  // The key of the hotkey that was pressed.
                //KeyModifier modifier = (KeyModifier)((int)m.LParam & 0xFFFF);       // The modifier of the hotkey that was pressed.
                int id = m.WParam.ToInt32();                                        // The id of the hotkey that was pressed.

                if (key == Keys.F1)
                {
                    Speech.Speak((this.godmodecheckbox.Checked ? "disabling godmode" : "enabling godmode"));
                    this.godmodecheckbox.PerformClick();
                } else if(key == Keys.F2)
                {
                    Speech.Speak(this.instakillcheckbox.Checked ? "disabling instakill" : "enabling instakill");
                    this.instakillcheckbox.PerformClick();
                } else if(key == Keys.F)
                {
                    Speech.Speak((this.flycheckbox.Checked ? "disabling fly hack" : "enabling fly hack"));
                    this.flycheckbox.PerformClick();
                }
            }
        }

        public bool HookStatus
        {
            get
            {
                return hooked;
            }
            set
            {
                if(value)
                {
                    this.hookedstatus.BackgroundImage = Resources.hooked_hooked;
                } else
                {
                    this.hookedstatus.BackgroundImage = Resources.hooked_unhooked;
                }
                this.hooked = value;
            }
        }

        private void minimize_hover_enter(object sender, EventArgs e)
        {
            this.minimizebtn.BackgroundImage = Resources.minimize_hover;
        }

        private void minimize_hover_leave(object sender, EventArgs e)
        {
            this.minimizebtn.BackgroundImage = Resources.minimize_non_hover;
        }

        private void closebtn_hover_enter(object sender, EventArgs e)
        {
            this.closebtn.BackgroundImage = Resources.close_hover;
        }

        private void closebtn_hover_leave(object sender, EventArgs e)
        {
            this.closebtn.BackgroundImage = Resources.close_non_hover;
        }

        private void closebtn_Click(object sender, EventArgs e)
        {
            //TODO: disable all cheats so caves get removed from the game.
            Application.Exit();
        }

        private void minimizebtn_Click(object sender, EventArgs e)
        {
            this.WindowState = FormWindowState.Minimized;
        }

        private void customCheckbox1_CheckedState(object sender, EventArgs e)
        {
            if (this.godmode == null)
            {
                this.godmode = new GodmodeCheat(this.m, this.godmodecheckbox);
            }

            if(this.instakillcheckbox.Checked)
            {
                this.instakillcheckbox.Checked = false;
                this.instakill.Disable();
            }

            if (this.godmodecheckbox.Checked)
            {
                if(this.HookStatus)
                {
                    this.godmode.Enable();
                } else
                {            
                    Error.ErrorNotHooked();
                    this.godmodecheckbox.Checked = false;
                }
            } else
            {
                this.godmode.Disable();
            }
        }

        private void hooksearch_DoWork(object sender, DoWorkEventArgs e)
        {
            while (true)
            {
                if(this.m.theProc == null || this.m.theProc.HasExited)
                {
                    int id = this.m.GetProcIdFromName("Spelunky");
                    bool b = this.m.OpenProcess(id);

                    if (b)
                    {
                        hooksearch.ReportProgress(1);
                    }
                    else
                    {
                        hooksearch.ReportProgress(0);
                    }
                }

                Thread.Sleep(3*60*60); //delay for 3 seconds
            }
        }

        private void hooksearch_ProgressChanged(object sender, ProgressChangedEventArgs e)
        {
            if(e.ProgressPercentage == 1)
            {
                Console.Beep(100, 80);
                this.HookStatus = true;
            } else
            {
                this.HookStatus = false;
            }
        }

        private void customCheckbox3_CheckedState(object sender, EventArgs e)
        {
            if(this.money == null)
            {
                this.money = new MoneyCheat(this.m, this.moneycheckbox);
            }

            if(this.moneycheckbox.Checked)
            {
                if(this.HookStatus)
                {
                    this.money.Enable();
                } else
                {
                    Error.ErrorNotHooked();
                    this.moneycheckbox.Checked = false;
                }
            } else
            {
                this.money.Disable();
            }
        }

        private void Window_FormClosing(object sender, FormClosingEventArgs e)
        {
            if(this.m.theProc.HasExited || this.m.theProc == null)
            {
                //update faster.
                this.HookStatus = false;
            }

            CheatRegistry.DisableAll(this);
        }

        private void bombcheckbox_CheckedState(object sender, EventArgs e)
        {
            if(this.bomb == null)
            {
                this.bomb = new BombCheat(this.m, this.bombcheckbox);
            }

            if(this.bombcheckbox.Checked)
            {
                if(this.HookStatus)
                {
                    this.bomb.Enable();
                } else
                {            
                    Error.ErrorNotHooked();
                    this.bombcheckbox.Checked = false;
                }
            } else
            {
                this.bomb.Disable();
            }
        }

        private void instakillcheckbox_CheckedState(object sender, EventArgs e)
        {
           if(this.instakill == null)
            {
                this.instakill = new InstakillCheat(this.m, this.instakillcheckbox);
            }

           if(this.godmodecheckbox.Checked)
            {
                this.godmodecheckbox.Checked = false;
                this.godmode.Disable();
            }

           if(this.instakillcheckbox.Checked)
            {
                if(this.HookStatus)
                {
                    this.instakill.Enable();
                } else
                {
                    Error.ErrorNotHooked();
                    this.instakillcheckbox.Checked = false;
                }
            } else
            {
                this.instakill.Disable();
            }
        }

        private void ropecheckbox_CheckedState(object sender, EventArgs e)
        {
            if(this.ropec == null)
            {
                this.ropec = new RopeCheat(this.m, this.ropecheckbox);
            }

            if(this.ropecheckbox.Checked)
            {
                if(this.HookStatus)
                {
                    this.ropec.Enable();
                } else
                {      
                    Error.ErrorNotHooked();
                    this.ropecheckbox.Checked = false;
                }
            } else
            {
                this.ropec.Disable();
            }
        }

        private void flycheckbox_CheckedState(object sender, EventArgs e)
        {
            if(this.flycheat == null)
            {
                this.flycheat = new FlyCheat(this.m, this.flycheckbox);
            }

            if(this.flycheckbox.Checked)
            {
                if(this.HookStatus)
                {
                    this.flycheat.Enable();
                } else
                {                
                    Error.ErrorNotHooked();
                    this.flycheckbox.Checked = false;
                }
            } else
            {
                this.flycheat.Disable();
            }
        }

        private void ghosttimercheckbox_CheckedState(object sender, EventArgs e)
        {
            if(this.ghost == null)
            {
                this.ghost = new GhostTimerCheat(this.m, this.ghosttimercheckbox);
            }

            if(this.ghosttimercheckbox.Checked)
            {
                if(this.HookStatus)
                {
                    this.ghost.Enable();
                } else
                {
                    Error.ErrorNotHooked();
                    this.ghosttimercheckbox.Checked = false;
                }
            } else
            {
                this.ghost.Disable();
            }
        }
    }
}
