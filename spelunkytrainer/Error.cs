﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace spelunkytrainer
{
    public class Error
    {

        public static void ErrorNotHooked()
        {
           MessageBox.Show("The trainer is not hooked", "error", MessageBoxButtons.OK, MessageBoxIcon.Error);
        }

        public static void ErrorAOBNotFound(string aob)
        {
            MessageBox.Show("the following aob was not found when enabling this cheat.\n\nAOB: "+aob+ "\n\nmake a ticket to https://gitlab.com/TeamRevive/csharp-trainers/spelunkytrainer/-/issues", "error", MessageBoxButtons.OK, MessageBoxIcon.Error);
        }

        public static void ErrorAddressNotFound()
        {
            MessageBox.Show("The code cave creation failed because the address was null", "error", MessageBoxButtons.OK, MessageBoxIcon.Error);
        }

        public static void ErrorEmptyCodeCave()
        {
            MessageBox.Show("The code cave creation failed because the bytes in the code cave where empty", "error", MessageBoxButtons.OK, MessageBoxIcon.Error);
        }

        public static void ErrorRestoreJMPBytesEmpty()
        {
            MessageBox.Show("The code cave creation failed because the bytes to restore the orginal instruction where empty", "error", MessageBoxButtons.OK, MessageBoxIcon.Error);
        }

        public static void ErrorCodeCaveCreationFailed()
        {
            MessageBox.Show("The code cave creation failed, the return pointer after code cave creation returned null", "error", MessageBoxButtons.OK, MessageBoxIcon.Error);
        }

    }
}
