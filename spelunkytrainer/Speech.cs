﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Speech.Synthesis;
using System.Text;
using System.Threading.Tasks;

namespace spelunkytrainer
{
    public class Speech
    {

        private static SpeechSynthesizer sp = new SpeechSynthesizer();

        public static void Speak(string text)
        {
            sp.SpeakAsyncCancelAll();
            sp.SpeakAsync(text);
        }

    }
}
