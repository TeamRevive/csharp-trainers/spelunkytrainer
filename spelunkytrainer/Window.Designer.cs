﻿namespace spelunkytrainer
{
    partial class Window
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(Window));
            this.header = new System.Windows.Forms.Panel();
            this.closebtn = new System.Windows.Forms.Panel();
            this.minimizebtn = new System.Windows.Forms.Panel();
            this.panel2 = new System.Windows.Forms.Panel();
            this.hookedstatus = new System.Windows.Forms.Panel();
            this.panel3 = new System.Windows.Forms.Panel();
            this.panel4 = new System.Windows.Forms.Panel();
            this.label8 = new System.Windows.Forms.Label();
            this.panel11 = new System.Windows.Forms.Panel();
            this.ghosttimercheckbox = new spelunkytrainer.controls.CustomCheckbox();
            this.muteCheckBox1 = new spelunkytrainer.controls.MuteCheckBox();
            this.label7 = new System.Windows.Forms.Label();
            this.panel10 = new System.Windows.Forms.Panel();
            this.flycheckbox = new spelunkytrainer.controls.CustomCheckbox();
            this.label6 = new System.Windows.Forms.Label();
            this.panel9 = new System.Windows.Forms.Panel();
            this.ropecheckbox = new spelunkytrainer.controls.CustomCheckbox();
            this.label5 = new System.Windows.Forms.Label();
            this.panel8 = new System.Windows.Forms.Panel();
            this.bombcheckbox = new spelunkytrainer.controls.CustomCheckbox();
            this.label4 = new System.Windows.Forms.Label();
            this.panel7 = new System.Windows.Forms.Panel();
            this.moneycheckbox = new spelunkytrainer.controls.CustomCheckbox();
            this.label3 = new System.Windows.Forms.Label();
            this.panel5 = new System.Windows.Forms.Panel();
            this.instakillcheckbox = new spelunkytrainer.controls.CustomCheckbox();
            this.label2 = new System.Windows.Forms.Label();
            this.panel6 = new System.Windows.Forms.Panel();
            this.godmodecheckbox = new spelunkytrainer.controls.CustomCheckbox();
            this.label1 = new System.Windows.Forms.Label();
            this.hooksearch = new System.ComponentModel.BackgroundWorker();
            this.moveableWindow1 = new spelunkytrainer.controls.MoveableWindow();
            this.fadeControl1 = new spelunkytrainer.controls.FadeControl();
            this.header.SuspendLayout();
            this.panel4.SuspendLayout();
            this.panel11.SuspendLayout();
            this.panel10.SuspendLayout();
            this.panel9.SuspendLayout();
            this.panel8.SuspendLayout();
            this.panel7.SuspendLayout();
            this.panel5.SuspendLayout();
            this.panel6.SuspendLayout();
            this.SuspendLayout();
            // 
            // header
            // 
            this.header.BackgroundImage = global::spelunkytrainer.Properties.Resources.headerbg;
            this.header.Controls.Add(this.closebtn);
            this.header.Controls.Add(this.minimizebtn);
            this.header.Dock = System.Windows.Forms.DockStyle.Top;
            this.header.Location = new System.Drawing.Point(0, 0);
            this.header.Name = "header";
            this.header.Size = new System.Drawing.Size(375, 33);
            this.header.TabIndex = 0;
            // 
            // closebtn
            // 
            this.closebtn.BackgroundImage = global::spelunkytrainer.Properties.Resources.close_non_hover;
            this.closebtn.Cursor = System.Windows.Forms.Cursors.Hand;
            this.closebtn.Location = new System.Drawing.Point(341, 0);
            this.closebtn.Name = "closebtn";
            this.closebtn.Size = new System.Drawing.Size(25, 33);
            this.closebtn.TabIndex = 1;
            this.closebtn.Click += new System.EventHandler(this.closebtn_Click);
            this.closebtn.MouseEnter += new System.EventHandler(this.closebtn_hover_enter);
            this.closebtn.MouseLeave += new System.EventHandler(this.closebtn_hover_leave);
            // 
            // minimizebtn
            // 
            this.minimizebtn.BackgroundImage = global::spelunkytrainer.Properties.Resources.minimize_non_hover;
            this.minimizebtn.Cursor = System.Windows.Forms.Cursors.Hand;
            this.minimizebtn.Location = new System.Drawing.Point(310, 0);
            this.minimizebtn.Name = "minimizebtn";
            this.minimizebtn.Size = new System.Drawing.Size(25, 33);
            this.minimizebtn.TabIndex = 0;
            this.minimizebtn.Click += new System.EventHandler(this.minimizebtn_Click);
            this.minimizebtn.MouseEnter += new System.EventHandler(this.minimize_hover_enter);
            this.minimizebtn.MouseLeave += new System.EventHandler(this.minimize_hover_leave);
            // 
            // panel2
            // 
            this.panel2.BackgroundImage = global::spelunkytrainer.Properties.Resources.game_picture;
            this.panel2.Location = new System.Drawing.Point(0, 33);
            this.panel2.Name = "panel2";
            this.panel2.Size = new System.Drawing.Size(121, 155);
            this.panel2.TabIndex = 1;
            // 
            // hookedstatus
            // 
            this.hookedstatus.BackgroundImage = global::spelunkytrainer.Properties.Resources.hooked_unhooked;
            this.hookedstatus.Location = new System.Drawing.Point(0, 187);
            this.hookedstatus.Name = "hookedstatus";
            this.hookedstatus.Size = new System.Drawing.Size(121, 32);
            this.hookedstatus.TabIndex = 2;
            // 
            // panel3
            // 
            this.panel3.BackgroundImage = global::spelunkytrainer.Properties.Resources.checkupdates;
            this.panel3.Cursor = System.Windows.Forms.Cursors.Hand;
            this.panel3.Location = new System.Drawing.Point(0, 256);
            this.panel3.Name = "panel3";
            this.panel3.Size = new System.Drawing.Size(121, 22);
            this.panel3.TabIndex = 4;
            // 
            // panel4
            // 
            this.panel4.BackColor = System.Drawing.Color.Transparent;
            this.panel4.BackgroundImage = global::spelunkytrainer.Properties.Resources.cheatbg;
            this.panel4.Controls.Add(this.label8);
            this.panel4.Controls.Add(this.panel11);
            this.panel4.Controls.Add(this.panel10);
            this.panel4.Controls.Add(this.panel9);
            this.panel4.Controls.Add(this.panel8);
            this.panel4.Controls.Add(this.panel7);
            this.panel4.Controls.Add(this.panel5);
            this.panel4.Controls.Add(this.panel6);
            this.panel4.Location = new System.Drawing.Point(121, 33);
            this.panel4.Name = "panel4";
            this.panel4.Size = new System.Drawing.Size(254, 349);
            this.panel4.TabIndex = 5;
            // 
            // label8
            // 
            this.label8.BackColor = System.Drawing.Color.Transparent;
            this.label8.Font = new System.Drawing.Font("Impact", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label8.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(159)))), ((int)(((byte)(160)))), ((int)(((byte)(161)))));
            this.label8.Location = new System.Drawing.Point(154, 322);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(88, 15);
            this.label8.TabIndex = 7;
            this.label8.Text = "Version: v1.0.0.0";
            // 
            // panel11
            // 
            this.panel11.BackgroundImage = global::spelunkytrainer.Properties.Resources.cheat_table_top;
            this.panel11.Controls.Add(this.ghosttimercheckbox);
            this.panel11.Controls.Add(this.label7);
            this.panel11.Location = new System.Drawing.Point(8, 223);
            this.panel11.Name = "panel11";
            this.panel11.Size = new System.Drawing.Size(234, 27);
            this.panel11.TabIndex = 6;
            // 
            // ghosttimercheckbox
            // 
            this.ghosttimercheckbox.BackColor = System.Drawing.Color.Transparent;
            this.ghosttimercheckbox.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("ghosttimercheckbox.BackgroundImage")));
            this.ghosttimercheckbox.Checked = false;
            this.ghosttimercheckbox.Cursor = System.Windows.Forms.Cursors.Hand;
            this.ghosttimercheckbox.Location = new System.Drawing.Point(181, -1);
            this.ghosttimercheckbox.MuteButton = this.muteCheckBox1;
            this.ghosttimercheckbox.Name = "ghosttimercheckbox";
            this.ghosttimercheckbox.Size = new System.Drawing.Size(46, 28);
            this.ghosttimercheckbox.TabIndex = 7;
            this.ghosttimercheckbox.CheckedState += new System.EventHandler(this.ghosttimercheckbox_CheckedState);
            // 
            // muteCheckBox1
            // 
            this.muteCheckBox1.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("muteCheckBox1.BackgroundImage")));
            this.muteCheckBox1.Checked = false;
            this.muteCheckBox1.Cursor = System.Windows.Forms.Cursors.Hand;
            this.muteCheckBox1.Location = new System.Drawing.Point(0, 220);
            this.muteCheckBox1.Name = "muteCheckBox1";
            this.muteCheckBox1.Size = new System.Drawing.Size(121, 31);
            this.muteCheckBox1.TabIndex = 3;
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.BackColor = System.Drawing.Color.Transparent;
            this.label7.Font = new System.Drawing.Font("Impact", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label7.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(159)))), ((int)(((byte)(160)))), ((int)(((byte)(161)))));
            this.label7.Location = new System.Drawing.Point(5, 5);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(104, 15);
            this.label7.TabIndex = 6;
            this.label7.Text = "F6: freeze ghost timer";
            // 
            // panel10
            // 
            this.panel10.BackgroundImage = global::spelunkytrainer.Properties.Resources.cheat_table_top;
            this.panel10.Controls.Add(this.flycheckbox);
            this.panel10.Controls.Add(this.label6);
            this.panel10.Location = new System.Drawing.Point(8, 188);
            this.panel10.Name = "panel10";
            this.panel10.Size = new System.Drawing.Size(234, 27);
            this.panel10.TabIndex = 5;
            // 
            // flycheckbox
            // 
            this.flycheckbox.BackColor = System.Drawing.Color.Transparent;
            this.flycheckbox.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("flycheckbox.BackgroundImage")));
            this.flycheckbox.Checked = false;
            this.flycheckbox.Cursor = System.Windows.Forms.Cursors.Hand;
            this.flycheckbox.Location = new System.Drawing.Point(181, -1);
            this.flycheckbox.MuteButton = this.muteCheckBox1;
            this.flycheckbox.Name = "flycheckbox";
            this.flycheckbox.Size = new System.Drawing.Size(46, 28);
            this.flycheckbox.TabIndex = 6;
            this.flycheckbox.CheckedState += new System.EventHandler(this.flycheckbox_CheckedState);
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.BackColor = System.Drawing.Color.Transparent;
            this.label6.Font = new System.Drawing.Font("Impact", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label6.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(159)))), ((int)(((byte)(160)))), ((int)(((byte)(161)))));
            this.label6.Location = new System.Drawing.Point(5, 5);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(50, 15);
            this.label6.TabIndex = 5;
            this.label6.Text = "F: fly hack";
            // 
            // panel9
            // 
            this.panel9.BackgroundImage = global::spelunkytrainer.Properties.Resources.cheat_table_top;
            this.panel9.Controls.Add(this.ropecheckbox);
            this.panel9.Controls.Add(this.label5);
            this.panel9.Location = new System.Drawing.Point(8, 155);
            this.panel9.Name = "panel9";
            this.panel9.Size = new System.Drawing.Size(234, 27);
            this.panel9.TabIndex = 4;
            // 
            // ropecheckbox
            // 
            this.ropecheckbox.BackColor = System.Drawing.Color.Transparent;
            this.ropecheckbox.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("ropecheckbox.BackgroundImage")));
            this.ropecheckbox.Checked = false;
            this.ropecheckbox.Cursor = System.Windows.Forms.Cursors.Hand;
            this.ropecheckbox.Location = new System.Drawing.Point(181, -1);
            this.ropecheckbox.MuteButton = this.muteCheckBox1;
            this.ropecheckbox.Name = "ropecheckbox";
            this.ropecheckbox.Size = new System.Drawing.Size(46, 28);
            this.ropecheckbox.TabIndex = 5;
            this.ropecheckbox.CheckedState += new System.EventHandler(this.ropecheckbox_CheckedState);
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.BackColor = System.Drawing.Color.Transparent;
            this.label5.Font = new System.Drawing.Font("Impact", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label5.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(159)))), ((int)(((byte)(160)))), ((int)(((byte)(161)))));
            this.label5.Location = new System.Drawing.Point(5, 4);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(94, 15);
            this.label5.TabIndex = 4;
            this.label5.Text = "F5: unlimited ropes";
            // 
            // panel8
            // 
            this.panel8.BackgroundImage = global::spelunkytrainer.Properties.Resources.cheat_table_top;
            this.panel8.Controls.Add(this.bombcheckbox);
            this.panel8.Controls.Add(this.label4);
            this.panel8.Location = new System.Drawing.Point(8, 122);
            this.panel8.Name = "panel8";
            this.panel8.Size = new System.Drawing.Size(234, 27);
            this.panel8.TabIndex = 3;
            // 
            // bombcheckbox
            // 
            this.bombcheckbox.BackColor = System.Drawing.Color.Transparent;
            this.bombcheckbox.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("bombcheckbox.BackgroundImage")));
            this.bombcheckbox.Checked = false;
            this.bombcheckbox.Cursor = System.Windows.Forms.Cursors.Hand;
            this.bombcheckbox.Location = new System.Drawing.Point(181, -1);
            this.bombcheckbox.MuteButton = this.muteCheckBox1;
            this.bombcheckbox.Name = "bombcheckbox";
            this.bombcheckbox.Size = new System.Drawing.Size(46, 28);
            this.bombcheckbox.TabIndex = 4;
            this.bombcheckbox.CheckedState += new System.EventHandler(this.bombcheckbox_CheckedState);
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.BackColor = System.Drawing.Color.Transparent;
            this.label4.Font = new System.Drawing.Font("Impact", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label4.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(159)))), ((int)(((byte)(160)))), ((int)(((byte)(161)))));
            this.label4.Location = new System.Drawing.Point(5, 4);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(98, 15);
            this.label4.TabIndex = 3;
            this.label4.Text = "F4: unlimited bombs";
            // 
            // panel7
            // 
            this.panel7.BackgroundImage = global::spelunkytrainer.Properties.Resources.cheat_table_top;
            this.panel7.Controls.Add(this.moneycheckbox);
            this.panel7.Controls.Add(this.label3);
            this.panel7.Location = new System.Drawing.Point(8, 89);
            this.panel7.Name = "panel7";
            this.panel7.Size = new System.Drawing.Size(234, 27);
            this.panel7.TabIndex = 2;
            // 
            // moneycheckbox
            // 
            this.moneycheckbox.BackColor = System.Drawing.Color.Transparent;
            this.moneycheckbox.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("moneycheckbox.BackgroundImage")));
            this.moneycheckbox.Checked = false;
            this.moneycheckbox.Cursor = System.Windows.Forms.Cursors.Hand;
            this.moneycheckbox.Location = new System.Drawing.Point(181, 0);
            this.moneycheckbox.MuteButton = this.muteCheckBox1;
            this.moneycheckbox.Name = "moneycheckbox";
            this.moneycheckbox.Size = new System.Drawing.Size(46, 28);
            this.moneycheckbox.TabIndex = 3;
            this.moneycheckbox.CheckedState += new System.EventHandler(this.customCheckbox3_CheckedState);
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.BackColor = System.Drawing.Color.Transparent;
            this.label3.Font = new System.Drawing.Font("Impact", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label3.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(159)))), ((int)(((byte)(160)))), ((int)(((byte)(161)))));
            this.label3.Location = new System.Drawing.Point(5, 5);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(160, 15);
            this.label3.TabIndex = 2;
            this.label3.Text = "F3: give 999999 on money pickup";
            // 
            // panel5
            // 
            this.panel5.BackgroundImage = global::spelunkytrainer.Properties.Resources.cheat_table_top;
            this.panel5.Controls.Add(this.instakillcheckbox);
            this.panel5.Controls.Add(this.label2);
            this.panel5.Location = new System.Drawing.Point(8, 56);
            this.panel5.Name = "panel5";
            this.panel5.Size = new System.Drawing.Size(234, 27);
            this.panel5.TabIndex = 1;
            // 
            // instakillcheckbox
            // 
            this.instakillcheckbox.BackColor = System.Drawing.Color.Transparent;
            this.instakillcheckbox.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("instakillcheckbox.BackgroundImage")));
            this.instakillcheckbox.Checked = false;
            this.instakillcheckbox.Cursor = System.Windows.Forms.Cursors.Hand;
            this.instakillcheckbox.Location = new System.Drawing.Point(181, -1);
            this.instakillcheckbox.MuteButton = this.muteCheckBox1;
            this.instakillcheckbox.Name = "instakillcheckbox";
            this.instakillcheckbox.Size = new System.Drawing.Size(46, 28);
            this.instakillcheckbox.TabIndex = 2;
            this.instakillcheckbox.CheckedState += new System.EventHandler(this.instakillcheckbox_CheckedState);
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.BackColor = System.Drawing.Color.Transparent;
            this.label2.Font = new System.Drawing.Font("Impact", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label2.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(159)))), ((int)(((byte)(160)))), ((int)(((byte)(161)))));
            this.label2.Location = new System.Drawing.Point(5, 5);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(112, 15);
            this.label2.TabIndex = 1;
            this.label2.Text = "F2: godmode + instakill";
            // 
            // panel6
            // 
            this.panel6.BackgroundImage = global::spelunkytrainer.Properties.Resources.cheat_table_top;
            this.panel6.Controls.Add(this.godmodecheckbox);
            this.panel6.Controls.Add(this.label1);
            this.panel6.Location = new System.Drawing.Point(8, 23);
            this.panel6.Name = "panel6";
            this.panel6.Size = new System.Drawing.Size(234, 27);
            this.panel6.TabIndex = 0;
            // 
            // godmodecheckbox
            // 
            this.godmodecheckbox.BackColor = System.Drawing.Color.Transparent;
            this.godmodecheckbox.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("godmodecheckbox.BackgroundImage")));
            this.godmodecheckbox.Checked = false;
            this.godmodecheckbox.Cursor = System.Windows.Forms.Cursors.Hand;
            this.godmodecheckbox.Location = new System.Drawing.Point(181, -2);
            this.godmodecheckbox.MuteButton = this.muteCheckBox1;
            this.godmodecheckbox.Name = "godmodecheckbox";
            this.godmodecheckbox.Size = new System.Drawing.Size(46, 28);
            this.godmodecheckbox.TabIndex = 1;
            this.godmodecheckbox.CheckedState += new System.EventHandler(this.customCheckbox1_CheckedState);
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.BackColor = System.Drawing.Color.Transparent;
            this.label1.Font = new System.Drawing.Font("Impact", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(159)))), ((int)(((byte)(160)))), ((int)(((byte)(161)))));
            this.label1.Location = new System.Drawing.Point(5, 6);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(63, 15);
            this.label1.TabIndex = 0;
            this.label1.Text = "F1: godmode";
            // 
            // hooksearch
            // 
            this.hooksearch.WorkerReportsProgress = true;
            this.hooksearch.DoWork += new System.ComponentModel.DoWorkEventHandler(this.hooksearch_DoWork);
            this.hooksearch.ProgressChanged += new System.ComponentModel.ProgressChangedEventHandler(this.hooksearch_ProgressChanged);
            // 
            // moveableWindow1
            // 
            this.moveableWindow1.ControlTargets = new System.Windows.Forms.Control[0];
            this.moveableWindow1.MainWindowTarget = this;
            // 
            // fadeControl1
            // 
            this.fadeControl1.IsDone = false;
            this.fadeControl1.IsRunning = false;
            this.fadeControl1.TargetForm = this;
            // 
            // Window
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.Purple;
            this.BackgroundImage = global::spelunkytrainer.Properties.Resources.background;
            this.ClientSize = new System.Drawing.Size(375, 382);
            this.Controls.Add(this.panel4);
            this.Controls.Add(this.panel3);
            this.Controls.Add(this.muteCheckBox1);
            this.Controls.Add(this.hookedstatus);
            this.Controls.Add(this.panel2);
            this.Controls.Add(this.header);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.Name = "Window";
            this.Opacity = 0D;
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Spelunky trainer";
            this.TransparencyKey = System.Drawing.Color.Purple;
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.Window_FormClosing);
            this.Load += new System.EventHandler(this.Window_Load);
            this.header.ResumeLayout(false);
            this.panel4.ResumeLayout(false);
            this.panel11.ResumeLayout(false);
            this.panel11.PerformLayout();
            this.panel10.ResumeLayout(false);
            this.panel10.PerformLayout();
            this.panel9.ResumeLayout(false);
            this.panel9.PerformLayout();
            this.panel8.ResumeLayout(false);
            this.panel8.PerformLayout();
            this.panel7.ResumeLayout(false);
            this.panel7.PerformLayout();
            this.panel5.ResumeLayout(false);
            this.panel5.PerformLayout();
            this.panel6.ResumeLayout(false);
            this.panel6.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Panel header;
        private System.Windows.Forms.Panel minimizebtn;
        private System.Windows.Forms.Panel closebtn;
        private System.Windows.Forms.Panel panel2;
        private System.Windows.Forms.Panel hookedstatus;
        private controls.MuteCheckBox muteCheckBox1;
        private System.Windows.Forms.Panel panel3;
        private System.Windows.Forms.Panel panel4;
        private System.Windows.Forms.Panel panel6;
        private System.Windows.Forms.Panel panel5;
        private System.Windows.Forms.Panel panel11;
        private System.Windows.Forms.Panel panel10;
        private System.Windows.Forms.Panel panel9;
        private System.Windows.Forms.Panel panel8;
        private System.Windows.Forms.Panel panel7;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.Label label8;
        private controls.CustomCheckbox instakillcheckbox;
        private controls.CustomCheckbox godmodecheckbox;
        private controls.CustomCheckbox moneycheckbox;
        private controls.CustomCheckbox ghosttimercheckbox;
        private controls.CustomCheckbox flycheckbox;
        private controls.CustomCheckbox ropecheckbox;
        private controls.CustomCheckbox bombcheckbox;
        private controls.MoveableWindow moveableWindow1;
        private controls.FadeControl fadeControl1;
        private System.ComponentModel.BackgroundWorker hooksearch;
    }
}

