﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using spelunkytrainer.Properties;
using System.Media;
using System.Security.Policy;
using System.IO;

namespace spelunkytrainer.controls
{
    public partial class CustomCheckbox : UserControl
    {
        private bool check = false;

        public event EventHandler CheckedState;


        public CustomCheckbox()
        {
            InitializeComponent();
        }

        public MuteCheckBox MuteButton
        {
            get;set;
        }

        public void PerformClick()
        {
            EventHandler handler = CustomCheckbox_Click;
            if(handler != null)
            {
                handler.Invoke(this, EventArgs.Empty);
            }
        }

        public bool Checked
        {
            get { return this.check; }
            set {
                if(value)
                {
                    this.BackgroundImage = Resources.checkbox_active;
                } else
                {
                    this.BackgroundImage = Resources.checkbox_deactive;
                }

                this.check = value;
            }
        }

        private void CustomCheckbox_Click(object sender, EventArgs e)
        {
            if(!this.Enabled)
            {
                return;
            }

            this.Checked = !this.Checked;

            if(this.MuteButton != null)
            {
                if (!this.MuteButton.Checked)
                {
                    if (this.Checked)
                    {
                        CheatActivate();
                    }
                    else
                    {
                        CheatDeActivate();
                    }
                }
            }

            EventHandler handler = CheckedState;
            if(handler != null)
            {
                handler.Invoke(sender, EventArgs.Empty);
            }
        }

        public void CheatActivate()
        {
            if(this.Visible)
            {
                Console.Beep();
            }  
        }

        public void CheatDeActivate()
        {
            if (this.Visible)
            {
                Console.Beep(100, 100);
            }
        }

        private void CustomCheckbox_EnabledChanged(object sender, EventArgs e)
        {
            if (!this.Enabled)
            {
                this.Cursor = Cursors.No;
                this.BackgroundImage = Resources.checkbox_disabled;
            }
            else
            {
                if (this.Checked)
                {
                    this.BackgroundImage = Resources.checkbox_active;
                }
                else
                {
                    this.BackgroundImage = Resources.checkbox_deactive;
                }
            }
        }
    }
}
