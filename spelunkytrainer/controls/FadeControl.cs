﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace spelunkytrainer.controls
{
    public partial class FadeControl : Component
    {
        private Form f;

        public event EventHandler FadeDone;

        private BackgroundWorker bg = new BackgroundWorker();
        private int progress = 0;

        public FadeControl()
        {
            InitializeComponent();
        }

        public void Start()
        {
            this.IsRunning = true;
            this.f.Opacity = 0; //hide
            bg.WorkerReportsProgress = true;
            bg.WorkerSupportsCancellation = true;
            bg.DoWork += new DoWorkEventHandler(timerdowork);
            bg.ProgressChanged += new ProgressChangedEventHandler(timerprogress);
            bg.RunWorkerCompleted += new RunWorkerCompletedEventHandler(timercomplete);

            this.bg.RunWorkerAsync();

        }

        private void timercomplete(object sender, RunWorkerCompletedEventArgs e)
        {
            EventHandler handler = FadeDone;
            if(handler != null)
            {
                handler?.Invoke(this, EventArgs.Empty);
            }
        }

        private void timerprogress(object sender, ProgressChangedEventArgs e)
        {
            f.Opacity += .01;
        }

        private void timerdowork(object sender, DoWorkEventArgs e)
        {
            while(true)
            {
                if(this.f.Opacity == 1.0)
                {
                    e.Cancel = true;
                    break;
                } else
                {
                    Task t = Task.Delay(05);
                    t.Wait();
                    this.progress = progress + 1;
                    this.bg.ReportProgress(this.progress);
                }
            }
        }

        public Form TargetForm
        {
            get
            {
                return this.f;
            }
            set
            {
                if(value == null)
                {
                    return;
                }
                this.f = value;
            }
        }

        public bool IsRunning
        {
            get; set;
        } = false;

        public bool IsDone
        {
            get; set;
        } = false;
    }
}
