﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using spelunkytrainer.Properties;

namespace spelunkytrainer.controls
{
    public partial class MuteCheckBox : UserControl
    {
        private bool checkd = false;
        public event EventHandler CheckedState;

        public MuteCheckBox()
        {
            InitializeComponent();
        }

        private void MuteCheckBox_Load(object sender, EventArgs e)
        {
            this.CheckedState += HandleCheckedState;
        }

        public bool Checked
        {
            get
            {
                return checkd;
            }
            set
            {
                this.checkd = value;
                EventHandler handler = CheckedState;
                if (handler != null)
                {
                    handler?.Invoke(this, EventArgs.Empty);
                }
            }
        }
         
        protected void HandleCheckedState(object sender, EventArgs args)
        {
            if(this.Checked)
            {
                this.BackgroundImage = Resources.muted_checked;
            } else
            {
                this.BackgroundImage = Resources.muted_unchecked;
            }
        }


        private void Checkbox_hover_enter(object sender, EventArgs e)
        {
            if(!this.Checked)
            {
                this.BackgroundImage = Resources.muted_hover;
            }
        }

        private void Checkbox_hover_leave(object sender, EventArgs e)
        {
            if (!this.Checked)
            {
                this.BackgroundImage = Resources.muted_unchecked;
            }
        }

        private void MuteCheckBox_Click(object sender, EventArgs e)
        {
            this.Checked = !this.Checked;
        }
    }
}
