﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace spelunkytrainer
{
    public class HotKeysz
    {

        [System.Runtime.InteropServices.DllImport("user32.dll")]
        private static extern bool RegisterHotKey(IntPtr hWnd, int id, int fsModifiers, int vk);
        [System.Runtime.InteropServices.DllImport("user32.dll")]
        private static extern bool UnregisterHotKey(IntPtr hWnd, int id);

        enum KeyModifier
        {
            None = 0,
            Alt = 1,
            Control = 2,
            Shift = 4,
            WinKey = 8
        }

        public static void RegisterKey(Window win, int key)
        {
            RegisterHotKey(win.Handle, key, (int)KeyModifier.None, key);
        }

        public static void UnregisterKey(Window win, int key)
        {
            UnregisterHotKey(win.Handle, key);
        }

    }
}
