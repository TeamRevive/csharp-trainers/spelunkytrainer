﻿using spelunkytrainer.Properties;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace spelunkytrainer
{
    static class Program
    {
        /// <summary>
        /// The main entry point for the application.
        /// </summary>
        [STAThread]
        static void Main()
        {

            AppDomain.CurrentDomain.AssemblyResolve += new ResolveEventHandler(delegate(object sender, ResolveEventArgs ev)
            {
                if(ev.Name.Substring(0, ev.Name.IndexOf(",")) == "Memory")
                {
                    //MessageBox.Show("found resource memory.dll");
                    //return Assembly.Load(Resources.Memory);
                } else if(ev.Name.Substring(0, ev.Name.IndexOf(",")) == ("System.Security.Principal.Windows"))
                {
                    return Assembly.Load(Resources.System_Security_Principal_Windows);
                }
                return null;
            });


            Application.EnableVisualStyles();
            Application.SetCompatibleTextRenderingDefault(false);
            Application.Run(new Window());
        }
    }
}
