﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace spelunkytrainer.cheats
{
    interface CheatFramework
    {

        bool Enable();

        void Disable();

    }
}
