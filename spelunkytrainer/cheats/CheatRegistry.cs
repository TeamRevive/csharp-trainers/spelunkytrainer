﻿using spelunkytrainer.controls;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace spelunkytrainer.cheats
{
    public class CheatRegistry
    {

        private static List<Cheat> cheats = new List<Cheat>();

        public static void Add(Cheat c)
        {
            cheats.Add(c);
        }

        public static void DisableAll(Window w)
        {
            if(!w.HookStatus)
            {
                foreach(Cheat cheat in cheats)
                {
                    cheat.CustomCheckBox().Checked = false;
                }
                return;
            }
            foreach(Cheat c in cheats)
            {
                if(c.IsActive())
                {
                    c.Restore();
                }
            }
        }

    }
}
