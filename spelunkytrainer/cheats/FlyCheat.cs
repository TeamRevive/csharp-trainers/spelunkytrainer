﻿using Memory;
using spelunkytrainer.controls;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace spelunkytrainer.cheats
{
    public class FlyCheat : CheatFramework
    {

        private Mem m;
        private Cheat cheat;

        public FlyCheat(Mem mem, CustomCheckbox cs)
        {
            this.m = mem;
            this.cheat = new Cheat(this.m, cs);
        }

        public bool Enable()
        {
            cheat.AobScan(0x00000000, 0xFFFFFFFF, "89 B8 48 02 00 00 8B 80 84 02 00 00")
            .AddCaveBytes(new byte[]
            {
                0xBF,0x54,0x31,0x14,0x3D,
                0x89,0xB8,0x48,0x02,0x00,
                0x00
            }).RestoreJumpInstructionBytes(new byte[] {
                0x89,0xB8,0x48,0x02,0x00,0x00
            });

            return cheat.Execute();
        }

        public void Disable()
        {
            cheat.Restore();
        }

    }
}
