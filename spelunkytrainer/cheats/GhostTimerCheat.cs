﻿using Memory;
using spelunkytrainer.controls;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace spelunkytrainer.cheats
{
    public class GhostTimerCheat : CheatFramework
    {

        private Mem m = new Mem();
        private Cheat cheat;
        private CustomCheckbox cs;

        public GhostTimerCheat(Mem m, CustomCheckbox cs)
        {
            this.m = m;
            this.cheat = new Cheat(this.m, cs);
            this.cs = cs;

        }

        public bool Enable()
        {
            this.cheat.AobScan(0x00000000, 0xFFFFFFFF, "BD 63 00 00 00 39")
            .AddCaveBytes(new byte[]
            {
                0xBD,0x00,0x00,0x00,0x00,
                0x89,0x91,0x40,0x59,0x44,
                0x00,0x89,0x91,0x44,0x59,
                0x44,0x00
            }).RestoreJumpInstructionBytes(new byte[] {
                0xBD,0x63,0x00,0x00,0x00
            });

            return this.cheat.Execute();
        }

        public void Disable()
        {
            this.cheat.Restore();
        }
    }
}
