﻿using Memory;
using spelunkytrainer.controls;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace spelunkytrainer.cheats
{
    public class MoneyCheat : CheatFramework
    {

        private Mem m;
        private Cheat cheat;
        private CustomCheckbox cs;

        public MoneyCheat(Mem m, CustomCheckbox cs)
        {
            this.cs = cs;
            this.m = m;
        }

        public bool Enable()
        {
            if(this.cheat == null)
            {
                this.cheat = new Cheat(this.m, this.cs);
            }

            this.cheat.AobScan(0x00000000, 0xFFFFFFFF, "01 81 2C 59 44 00").
            AddCaveBytes(new byte[] {
                0xB8,0x3F,0x42,0x0F,0x00,      // mov eax, (int)999999 //move money value into eax
                0x01,0x81,0x2C,0x59,0x44,0x00, // add [ecx+0044592C],eax //add eax to address
            }).RestoreJumpInstructionBytes(new byte[]
            {
                0x01,0x81,0x2C,0x59,0x44,0x00 //Spelunky.exe+52E7E - 01 81 2C594400        - add [ecx+0044592C],eax
            });

            return this.cheat.Execute();
        }

        public void Disable()
        {
            this.cheat.Restore();
        }

    }
}
