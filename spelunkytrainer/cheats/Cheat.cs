﻿using Memory;
using spelunkytrainer.controls;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace spelunkytrainer.cheats
{
    public class Cheat
    {

        private string address;
        private CustomCheckbox cs;


        public Cheat(Mem m, CustomCheckbox c)
        {
            this.cs = c;
            GetMemory = m;
            CheatRegistry.Add(this);
        }

        public Mem GetMemory
        {
            get;set;
        }

        public CustomCheckbox CustomCheckBox()
        {
            return this.cs;
        }

        private int ReplaceCount
        {
            get;set;
        }

        private string Address
        {
            get {
               if(this.address == "")
                {
                    throw new Exception("no address found, did the aob result?");
                }
                return this.address;
            }
            set
            {
                this.address = value;
            }
        }

        private byte[] RestoreInstructionJumpBytes
        {
            get;set;
        }

        private byte[] CaveBytes
        {
            get;set;
        }

        private UIntPtr AllocatedAddress
        {
            get;set;
        }

        public bool IsActive()
        {
            if(Address != null)
            {
                byte[] bytes = GetMemory.ReadBytes(Address, RestoreInstructionJumpBytes.Length);
                if(!bytes.SequenceEqual(RestoreInstructionJumpBytes))
                {
                    return true;
                }
            }

            return false;
        }

        public Cheat AobScan(string aob, bool writeable = false, bool executable = true)
        {
            Task<IEnumerable<long>> t = GetMemory.AoBScan(aob, writeable, executable);
            t.Wait();
            String address = t.Result.FirstOrDefault().ToString("X");

            Address = address;

            if(Address == "0")
            {
                Error.ErrorAOBNotFound(aob);
                this.CustomCheckBox().Checked = false;
            }

            return this;

        }

        public Cheat AobScan(long start, long end, string aob, bool writeable = false, bool executable = true)
        {
            Task<IEnumerable<long>> t = GetMemory.AoBScan(start, end, aob, writeable, executable);
            t.Wait();
            String address = t.Result.FirstOrDefault().ToString("X");

            Address = address;

            if (Address == "0")
            {
                Error.ErrorAOBNotFound(aob);
                this.CustomCheckBox().Checked = false;
            }

            return this;

        }

        [Obsolete("this function is more used for debugging, not for production :)")]
        public Cheat SetReplaceCount(int bytecount)
        {
            this.ReplaceCount = bytecount;
            return this;
        }

        public Cheat RestoreJumpInstructionBytes(byte[] bytes)
        {
            RestoreInstructionJumpBytes = bytes;
            return this;
        }

        public Cheat AddCaveBytes(byte[] bytes)
        {
            CaveBytes = bytes;

            return this;
        }

        public bool Execute()
        {
            //first check if minimal requirements is good enough...

            if(Address == "")
            {
                Error.ErrorAddressNotFound();
                this.CustomCheckBox().Checked = false;
            }

            if(CaveBytes == null || CaveBytes.Count() == 0)
            {
                Error.ErrorEmptyCodeCave();
                this.CustomCheckBox().Checked = false;
            }

            if (RestoreInstructionJumpBytes == null || RestoreInstructionJumpBytes.Count() == 0)
            {
                Error.ErrorRestoreJMPBytesEmpty();
                this.CustomCheckBox().Checked = false;
            }

            UIntPtr ptr = this.GetMemory.CreateCodeCave(Address, CaveBytes, RestoreInstructionJumpBytes.Length);
            AllocatedAddress = ptr;

            if(ptr != UIntPtr.Zero)
            {
                return true;
            } else
            {
                Error.ErrorCodeCaveCreationFailed();
                this.CustomCheckBox().Checked = false;
            }
            return false;
        }

        public bool Restore()
        {
            if(!((Window)this.CustomCheckBox().ParentForm).HookStatus)
            {
                return false;
            }

            byte[] restorejmp = RestoreInstructionJumpBytes;

            Memory.Mem.VirtualFreeEx(this.GetMemory.pHandle, this.AllocatedAddress, (UIntPtr)0, 0x8000);
            this.GetMemory.WriteBytes(this.Address, restorejmp);

            return true;

        }

    }
}
