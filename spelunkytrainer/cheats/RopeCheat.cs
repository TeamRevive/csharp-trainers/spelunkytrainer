﻿using Memory;
using spelunkytrainer.controls;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace spelunkytrainer.cheats
{
    public class RopeCheat : CheatFramework
    {

        private Mem m;
        private Cheat cheat;
        private CustomCheckbox cs;

        public RopeCheat(Mem m, CustomCheckbox cs)
        {
            this.m = m;
            this.cheat = new Cheat(this.m, cs);
            this.cs = cs;
        }

        public bool Enable()
        {
            this.cheat.AobScan(0x00000000, 0xFFFFFFFF, "FF 48 14 68 DC 9B ?? 00")
            .AddCaveBytes(new byte[] {

                0x81,0x40,0x14,0x99,0x00,
                0x00,0x00,0x68,0xDC,0x9B,
                0x1D,0x00

            }).RestoreJumpInstructionBytes(new byte[] {
                0xFF,0x48,0x14,0x68,0xDC,
                0x9B,0xAC,0x00
            });

            return this.cheat.Execute();
        }

        public void Disable()
        {
            this.cheat.Restore();
        }

    }
}
