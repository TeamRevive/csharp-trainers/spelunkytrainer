﻿using Memory;
using spelunkytrainer.controls;
using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml;

namespace spelunkytrainer.cheats
{
    public class GodmodeCheat : CheatFramework
    {

        private Mem m;
        private Cheat cheat;
        private CustomCheckbox cs;

        public GodmodeCheat(Mem m, CustomCheckbox cs)
        {
            this.cs = cs;
            this.m = m;
        }

        public bool Enable()
        {
            if (cheat == null)
            {
                this.cheat = new Cheat(this.m, this.cs);
            }

            cheat.
                AobScan(0x00000000, 0xFFFFFFFF, "89 8E 40 01 00 00 3D").
                AddCaveBytes(new byte[] {
                        //note to self 0x57 is a jmp  to, and 0x90 is a NOP (No operation opcode)
                    
                        0x83,0xF8,0x00,0x74,0x1A,
                        0x0F,0x1F,0x40,0x00,0x75,
                        0x09,0x0F,0x1F,0x40,0x00,
                        0xE9,0xB3,0x59,0x02,0x00,
                        0x89,0x8E,0x40,0x01,0x00,
                        0x00,0xE9,0xA8,0x59,0x02,
                        0x00,0xC7,0x86,0x40,0x01,
                        0x00,0x00,0x14,0x00,0x00,
                        0x00
                    }).
                RestoreJumpInstructionBytes(new byte[] {
                    0x89,0x8E,0x40,0x01,0x00,0x00
                });

                //execute the cheat.
                cheat.Execute();
            return true;
        }

        public void Disable()
        {
            //clean up the code cave.
            cheat.Restore();
        }

    }
}
