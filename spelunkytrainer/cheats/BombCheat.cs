﻿using Memory;
using spelunkytrainer.controls;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace spelunkytrainer.cheats
{
    public class BombCheat : CheatFramework
    {

        private Mem m;
        private Cheat bombcheat;
        private CustomCheckbox cs;

        public BombCheat(Mem m, CustomCheckbox cs)
        {
            this.cs = cs;
            this.m = m;
        }

        public bool Enable()
        {
            if(this.bombcheat == null)
            {
                this.bombcheat = new Cheat(this.m, this.cs);
            }

            /*
              with a aob signature we scan for bytes inside a function table to find our function address
              the required function fires when the player picks up bombs or use bombs, with other words this cheat does not give you instantly 99 bombs.
              we do this because if we would give instantly 99 bombs we either need a different function which constant runs which get shared with other functions
              which lead us to some intercompatibilitys, or use a pointer or aob to the display value... this isn't really a viable option when we aim to make this cheat surviving for game updates.

              this replaces the function and jump to our generated section of code where we add 99 bombs as soon a player picks up a bomb or uses one
              and then it jump back to normal code, this is what code caving is supposed to be.
             */

            this.bombcheat.
            AobScan(0x00000000, 0xFFFFFFFF, "0F 8E 78 01 00 00").AddCaveBytes(new byte[]
            {

                0xC7,0x40,0x10,0x64,0x00,
                0x00,0x00

            }).RestoreJumpInstructionBytes(new byte[] {
                0x0F,0x8E,0x78,0x01,0x00,0x00 //it replaces 8 bytes we saw this by using our signature creator, since the instruction is less than 5 for a jump we steal bytes from the next instruction
            });


            return this.bombcheat.Execute();

        }

        public void Disable()
        {
            this.bombcheat.Restore();
        }
    }
}
